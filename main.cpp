#include <iostream>
#include <mpi/mpi.h>

using namespace std;

const int SIZE = 100000;


int *getPart(int *arr, int start, int end) {
    int *new_arr = new int[end - start];
    int j = 0;

    for (int i = start; i < end; i++) {
        new_arr[j] = arr[i];
        j++;
    }

    return new_arr;
}

int *bubbleSort(int *arr, int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < i; j++) {
            if (arr[i] > arr[j]) {
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }

    for (int i = 0; i < size; i++) {
        cout << arr[i] << endl;
    }
    return arr;
}

int *combine(int *arr1, int *arr2, int size1, int size2) {
    int *new_arr = new int[size1 + size2];

    int j = 0;
    for (int i = 0; i < size1; i++) {
        new_arr[j] = arr1[i];
        j++;
    }
    for (int i = 0; i < size2; i++) {
        new_arr[j] = arr2[i];
        j++;
    }

    return new_arr;
}

int *getRandom(int size) {
    int *arr = new int[size];

    for (int i = 0; i < size; i++) {
        arr[i] = rand() % size;
    }

    return arr;
}

int *
exchangeBig(int rank, bool notFullyDivided, int j, int procSize, int blockSize, MPI::Status &status, int *recvArray) {
    if (notFullyDivided && j + 1 == procSize - 1) {
        int *tmpArray = new int[blockSize + SIZE % procSize];
        MPI::COMM_WORLD.Recv(tmpArray, blockSize + SIZE % procSize, MPI::INT, j + 1, MPI::ANY_TAG,
                             status);
        int *mergedArray = combine(recvArray, tmpArray, blockSize, blockSize + SIZE % procSize);
        mergedArray = bubbleSort(mergedArray, blockSize + blockSize + SIZE % procSize);
        recvArray = getPart(mergedArray, 0, blockSize);

    } else {
        int *tmpArray = new int[blockSize];
        MPI::COMM_WORLD.Recv(tmpArray, blockSize, MPI::INT, j + 1, MPI::ANY_TAG, status);
        int *mergedArray = combine(recvArray, tmpArray, blockSize, blockSize);
        mergedArray = bubbleSort(mergedArray, 2 * blockSize);
        recvArray = getPart(mergedArray, 0, blockSize);
    }

    return recvArray;
}

int *
exchangeSmall(int rank, bool notFullyDivided, int j, int procSize, int blockSize, MPI::Status &status, int *recvArray) {
    int *tmpArray = new int[blockSize];

    MPI::COMM_WORLD.Recv(tmpArray, blockSize, MPI::INT, j, MPI::ANY_TAG, status);

    if (notFullyDivided && rank == procSize - 1) {
        int *mergedArray = combine(recvArray, tmpArray, blockSize + SIZE % procSize, blockSize);
        mergedArray = bubbleSort(mergedArray, blockSize + blockSize + SIZE % procSize);
        recvArray = getPart(mergedArray, blockSize, 2 * blockSize + SIZE % procSize);

    } else {
        int *mergedArray = combine(recvArray, tmpArray, blockSize, blockSize);
        mergedArray = bubbleSort(mergedArray, 2 * blockSize);
        recvArray = getPart(mergedArray, blockSize, 2 * blockSize);
    }


    return recvArray;
}


void output(bool notFullyDivided, int procSize, int rank, int blockSize, int *recvArray, double timer) {
    for (int i = 0; i < procSize; i++) {
        MPI::COMM_WORLD.Barrier();

        if (rank == i) {

            printf("PROCESSOR %d\n", rank);

            if (notFullyDivided && rank == procSize - 1) {
                for (int j = 0; j < blockSize + SIZE % procSize; j++) {
                    printf("%d\n", recvArray[j]);
                }
            } else {
                for (int j = 0; j < blockSize; j++) {
                    printf("%d\n", recvArray[j]);
                }
            }

        }
    }

    cout << "Sort time : " << timer << endl;
}

int *preparePhase(bool notFullyDivided, int rank, int procSize, int blockSize, MPI::Status &status, int *arr) {

    int **senderParts = nullptr;
    int *recvArray = nullptr;

    if (rank != 0 && rank != procSize - 1) { //Get from root node
        recvArray = new int[blockSize];
        MPI::COMM_WORLD.Recv(recvArray, blockSize, MPI::INT, 0, MPI::ANY_TAG, status);
    } else if (rank == procSize - 1 && rank != 0) {
        if (notFullyDivided) {
            recvArray = new int[blockSize + SIZE % procSize];
            MPI::COMM_WORLD.Recv(recvArray, blockSize + SIZE % procSize, MPI::INT, 0, MPI::ANY_TAG, status);
        } else {
            recvArray = new int[blockSize];
            MPI::COMM_WORLD.Recv(recvArray, blockSize, MPI::INT, 0, MPI::ANY_TAG, status);
        }
    } else { //Root node send parts of array to every node
        senderParts = new int *[procSize];
        for (int i = 0; i < procSize - 1; i++) {
            senderParts[i] = new int[blockSize];
        }
        if (notFullyDivided) {
            senderParts[procSize - 1] = new int[blockSize + SIZE % procSize];
        } else {
            senderParts[procSize - 1] = new int[blockSize];
        }
        for (int i = 0; i < procSize - 1; i++) {
            senderParts[i] = getPart(arr, i * blockSize, i * blockSize + blockSize);
        }
        senderParts[procSize - 1] = getPart(arr, (procSize - 1) * blockSize, SIZE - 1);
        if (procSize != 1) {
            for (int i = 1; i < procSize - 1; i++) {
                MPI::COMM_WORLD.Isend(senderParts[i], blockSize, MPI::INT, i, 0);
            }
            if (notFullyDivided) {
                MPI::COMM_WORLD.Isend(senderParts[procSize - 1], blockSize + SIZE % procSize, MPI::INT, procSize - 1,
                                      0);
            } else {
                MPI::COMM_WORLD.Isend(senderParts[procSize - 1], blockSize, MPI::INT, procSize - 1, 0);
            }
        }
        recvArray = new int[blockSize];
        recvArray = senderParts[0]; //Array for the Root Node
    }

    return recvArray;
}

int main(int argc, char *argv[]) {

    int *arr = getRandom(SIZE); // Generate random array with SIZE
    MPI::Status status;
    int procSize = 0;
    int rank = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI::COMM_WORLD, &procSize); //Get number of processors
    MPI_Comm_rank(MPI::COMM_WORLD, &rank); //Get rank
    int blockSize = SIZE / procSize; //Get block size
    bool notFullyDivided = (SIZE % procSize != 0);
    int *recvArray = preparePhase(notFullyDivided, rank, procSize, blockSize, status, arr); //Get array
    cout << "\nPreparation phase end\n" << endl;

    MPI::COMM_WORLD.Barrier();

    double timer = MPI_Wtime();

    if (procSize == 1) {
        recvArray = bubbleSort(recvArray, SIZE); //Just sort if we have only one processor
    }

    //Communication phase
    for (int i = 0; i < procSize; i++) {
        if (i % 2 == 0) {
            for (int j = 0; j < procSize - 1; j += 2) {

                if (rank == j) {
                    MPI::COMM_WORLD.Isend(recvArray, blockSize, MPI::INT, j + 1, 0);
                    recvArray = exchangeBig(rank, notFullyDivided, j, procSize, blockSize, status, recvArray);
                } else if (rank == j + 1) {
                    if (notFullyDivided && rank == procSize - 1) {
                        MPI::COMM_WORLD.Isend(recvArray, blockSize + SIZE % procSize, MPI::INT, j, 0);
                    } else {
                        MPI::COMM_WORLD.Isend(recvArray, blockSize, MPI::INT, j, 0);
                    }
                    recvArray = exchangeSmall(rank, notFullyDivided, j, procSize, blockSize, status, recvArray);
                }


            }
        } else {
            for (int j = 1; j < procSize - 1; j += 2) {
                if (rank == j) {
                    MPI::COMM_WORLD.Isend(recvArray, blockSize, MPI::INT, j + 1, 0);
                    recvArray = exchangeBig(rank, notFullyDivided, j, procSize, blockSize, status, recvArray);
                } else if (rank == j + 1) {
                    if (notFullyDivided && rank == procSize - 1) {
                        MPI::COMM_WORLD.Isend(recvArray, blockSize + SIZE % procSize, MPI::INT, j, 0);
                    } else {
                        MPI::COMM_WORLD.Isend(recvArray, blockSize, MPI::INT, j, 0);
                    }
                    recvArray = exchangeSmall(rank, notFullyDivided, j, procSize, blockSize, status, recvArray);

                }
            }
        }
    }
    cout << "\nCommunication phase end\n" << endl;

    timer = MPI_Wtime() - timer;


    //Output
    MPI::COMM_WORLD.Barrier();
    output(notFullyDivided, procSize, rank, blockSize, recvArray, timer);
    MPI::COMM_WORLD.Barrier();
    MPI_Finalize();

    return 0;
}
